FROM adoptopenjdk/openjdk11:latest
RUN apt-get update && apt-get install -y netcat
COPY target/*.jar store-management.jar
#COPY entrypoint.sh /usr/bin/entrypoint.sh
#RUN chmod u+x /usr/bin/entrypoint.sh
#RUN apt-get update && apt-get install -y netcat
#ENTRYPOINT ["/usr/bin/entrypoint.sh"]
CMD ["java","-jar","store-management.jar"]