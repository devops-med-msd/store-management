host="dc-store-management-mysql"
port="3306"
while ! nc -z "$host" "$port"; do
    echo "Mysql  is unavailable - sleeping";
    sleep 3;
done
echo "$host is up - executing command"
cmd java -jar store-management.jar
